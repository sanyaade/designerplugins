<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>PlotlyPlot</name>
    <message>
        <source>Started Loading...</source>
        <translation type="unfinished">Laden starten...</translation>
    </message>
    <message>
        <source>Error loading the content.</source>
        <translation type="unfinished">Fehler beim Laden des Inhalts.</translation>
    </message>
    <message>
        <source>Loading (%1 %)</source>
        <translation type="unfinished">Laden (%1 %)</translation>
    </message>
    <message>
        <source>File Location</source>
        <translation type="unfinished">Dateiort</translation>
    </message>
    <message>
        <source>Image (*.%1)</source>
        <translation type="unfinished">Bild (*.%1)</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>itom widget for plotly figures.</source>
        <translation type="unfinished">itom-Widget für Plotly-Grafiken.</translation>
    </message>
    <message>
        <source>LGPL</source>
        <translation></translation>
    </message>
</context>
</TS>
